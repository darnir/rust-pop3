# Rust POP3

This is a simple application I started writing in Rust as a way to learn the
language. I wanted a CLI application that can access a POP3 server and download
emails, with two specific requirements:

1. It supports the `APOP` authentication command
2. It allows the password to be pulled from an external command

## Usage
This application is currently severely limited and doesn't perform all
functions. However, it can log in, get a list of all available messages and
even download a mail from the server. However, it currently can't store a mail
to the hard disk. I could not find a good maildir library for Rust, so I will
try and write one myself.

Currently, this application requires that a configuration file exist at
`$XDG_CONFIG_HOME/getpop/config.ini`. The configuration file should look like
this:

```
[test]
host=localhost
port=8000
auth=Plain
username=testuser
exec_password=/usr/bin/pass Accounts/UdS/password
```

Use a valid POP3 server and credentials (exec_password could simply be `echo`
if you want to store the password in plaintext)

Then, the right account name should be passed to the `read_config()` function
to initialize an account.
