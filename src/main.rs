#[macro_use]
extern crate lazy_static;
extern crate ini;
extern crate xdg;
extern crate openssl;
extern crate regex;
extern crate crypto;

use std::fs::File;
use std::io::{Read, Result, Error, ErrorKind, Write, BufReader, BufRead};
use std::net::TcpStream;
use std::path::PathBuf;
use crypto::md5::Md5;
use crypto::digest::Digest;
use std::process::{Command, Stdio};
use openssl::ssl::{SslMethod, SslConnectorBuilder, SslStream};
use regex::Regex;
use ini::Ini;

struct AccountConfig {
    host: String,
    port: u16,
    username: String,
    password: String,
    auth: String,
    maildir: PathBuf,
}

struct POP3Connection {
    account: AccountConfig,
    stream: TCPStream,
    reader: TCPReader,
    state: POP3State,
    timestamp: String,
}

enum TCPReader {
    Plain(BufReader<TcpStream>),
    SSL(BufReader<SslStream<TcpStream>>),
}

enum TCPStream {
    Plain(TcpStream),
    SSL(SslStream<TcpStream>),
}

impl Read for TCPReader {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        match *self {
            TCPReader::Plain(ref mut stream) => stream.read(buf),
            TCPReader::SSL(ref mut stream) => stream.read(buf),
        }
    }
}

impl TCPReader {
    fn read_until(&mut self, byte: u8, buf: &mut Vec<u8>) -> Result<usize> {
        match *self {
            TCPReader::Plain(ref mut stream) => stream.read_until(byte, buf),
            TCPReader::SSL(ref mut stream) => stream.read_until(byte, buf),
        }
    }
}

impl Write for TCPStream {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        match *self {
            TCPStream::Plain(ref mut stream) => stream.write(buf),
            TCPStream::SSL(ref mut stream) => stream.write(buf),
        }
    }

    fn flush(&mut self) -> Result<()> {
        match *self {
            TCPStream::Plain(ref mut stream) => stream.flush(),
            TCPStream::SSL(ref mut stream) => stream.flush(),
        }
    }
}

impl TCPStream {
    fn write_string(&mut self, buf: &str) -> Result<usize> {
        self.write(format!("{}\r\n", buf).as_ref())
    }
}

struct EmailMetadata {
    msg_id: u32,
    msg_size: u32,
}

enum POP3Data {
    LOGIN,
    STAT { mbox_size: u32, mbox_num: u32 },
    LIST { mailbox: Vec<EmailMetadata> },
    RETR { msg_data: String },
}

impl POP3Data {
    fn parse_stat(stat_line: &str) -> POP3Data {
        lazy_static!{
            static ref STAT_REGEX: Regex = Regex::new(r"(?P<nmsg>\d+) (?P<size>\d+)").unwrap();
        }
        let stat_cap = STAT_REGEX.captures(stat_line).unwrap();

        POP3Data::STAT {
            mbox_num: stat_cap.name("nmsg").unwrap().parse::<u32>().unwrap(),
            mbox_size: stat_cap.name("size").unwrap().parse::<u32>().unwrap(),
        }
    }

    fn parse_list(list_data: &[String]) -> POP3Data {
        lazy_static!{
            static ref LIST_REGEX: Regex = Regex::new(r"^(?P<msgid>\d+) (?P<msgsize>\d+)").unwrap();
        }

        let mut mbox: Vec<EmailMetadata> = Vec::new();
        for line in list_data[1..].iter() {
            let cap = LIST_REGEX.captures(line).unwrap();
            mbox.push(EmailMetadata {
                msg_id: cap.name("msgid").unwrap().parse::<u32>().unwrap(),
                msg_size: cap.name("msgsize").unwrap().parse::<u32>().unwrap(),
            });
        }
        POP3Data::LIST { mailbox: mbox }
    }

    fn parse_retr(retr_data: &[String]) -> POP3Data {
        let mut data = String::new();
        for line in retr_data[1..].iter() {
            data.push_str(line);
        }
        POP3Data::RETR { msg_data: data }
    }
}

#[derive(PartialEq)]
enum POP3State {
    BEGIN,
    AUTHORIZATION,
    TRANSACTION,
    UPDATE,
}

fn read_config(name: &str) -> AccountConfig {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("getpop").unwrap();
    let config_path = xdg_dirs.place_config_file("config.ini")
        .expect("Cannot create configuration directory");
    let conf = Ini::load_from_file(config_path.to_str().unwrap())
        .expect("Unable to load configuration file");
    let section = conf.section(Some(name).to_owned())
        .expect("No [Account] section found in config.ini");
    let username = section.get("username")
        .expect("Username field missing in config.ini");
    let password_exec = section.get("exec_password")
        .expect("No command line provided to get password");
    let host = section.get("host")
        .expect("Host field missing in config.ini");
    let port = section.get("port")
        .expect("Port field missing in config.ini")
        .parse::<u16>()
        .expect("Unable to parse port to a numeric value");
    let auth = section.get("auth")
        .expect("No auth found");

    let cmd_line: Vec<&str> = password_exec.split(' ').collect();
    let mut output = Command::new(cmd_line[0])
        .args(&cmd_line[1..])
        .stdout(Stdio::piped())
        .spawn()
        .expect("Unable to execute command line");
    let mut password_buff = BufReader::new(output.stdout.as_mut().unwrap());
    let mut password = String::new();
    password_buff.read_line(&mut password).unwrap();
    password.pop(); // Eliminate trailing newline

    let maildir_path = xdg_dirs.create_data_directory(name).unwrap();
    xdg_dirs.create_data_directory(format!("{}/new", name)).unwrap();
    xdg_dirs.create_data_directory(format!("{}/cur", name)).unwrap();
    xdg_dirs.create_data_directory(format!("{}/tmp", name)).unwrap();

    AccountConfig {
        host: host.to_string(),
        port: port,
        username: username.to_string(),
        password: password,
        auth: auth.to_string(),
        maildir: maildir_path,
    }
}

impl POP3Connection {
    fn new(account: AccountConfig) -> Result<POP3Connection> {
        let tcp_stream = TcpStream::connect((&account.host[..], account.port)).unwrap();
        let mut ctx = match account.auth.as_ref() {
            "Plain" => {
                POP3Connection {
                    account: account,
                    stream: TCPStream::Plain(tcp_stream.try_clone().unwrap()),
                    reader: TCPReader::Plain(BufReader::new(tcp_stream)),
                    state: POP3State::BEGIN,
                    timestamp: String::new(),
                }
            }
            "SSL" => {
                let connector = SslConnectorBuilder::new(SslMethod::tls()).unwrap().build();
                let stream = connector.connect(&account.host[..], tcp_stream.try_clone().unwrap())
                    .unwrap();
                POP3Connection {
                    stream: TCPStream::SSL(stream),
                    reader: TCPReader::SSL(BufReader::new(connector.clone()
                        .connect(&account.host[..], tcp_stream)
                        .unwrap())),
                    account: account,
                    state: POP3State::BEGIN,
                    timestamp: String::new(),
                }
            }
            _ => return Err(Error::new(ErrorKind::Other, "Unknown auth type")),

        };
        ctx.read_greeting();
        ctx.state = POP3State::AUTHORIZATION;
        Ok(ctx)
    }

    fn read_greeting(&mut self) {
        let greet = &self.read_response(false, 0).unwrap()[0];
        let re = Regex::new(r"(<.*>)\r\n$").unwrap();
        for cap in re.captures_iter(greet) {
            self.timestamp = cap.at(1).unwrap().to_string();
        }
    }

    fn read_response(&mut self, is_multiline: bool, size: u32) -> Result<Vec<String>> {
        let mut response_data: Vec<String> = Vec::new();
        let cr = 0x0d;
        let lf = 0x0a;
        let mut complete = false;

        // First read the status line
        let mut buff = Vec::new();
        self.reader.read_until(lf, &mut buff).unwrap();
        let line = String::from_utf8(buff.clone()).unwrap();
        response_data.push(line);
        print!("{}", response_data[0]);

        while !complete && is_multiline {
            buff.clear();
            self.reader.read_until(lf, &mut buff).unwrap();
            let line = unsafe { String::from_utf8_unchecked(buff.clone()) };
            if line == ".\r\n" {
                complete = true;
            } else {
                // Don't add the final .CRLF to the response
                response_data.push(line);
            }
        }

        Ok(response_data)
    }

    fn get_apop_digest(&mut self) -> String {
        let mut hasher = Md5::new();
        hasher.input_str(&self.timestamp);
        hasher.input_str(&self.account.password);
        hasher.result_str()
    }

    fn stat(&mut self) -> Result<POP3Data> {
        assert!(self.state == POP3State::TRANSACTION);
        self.send_command("STAT", None, None).map(|msg| POP3Data::parse_stat(&msg[0]))
    }

    fn login(&mut self) -> Result<POP3Data> {
        assert!(self.state == POP3State::AUTHORIZATION);
        let username = &self.account.username.clone();
        let auth_user_cmd = self.send_command("USER", Some(username), None);
        let auth_response;
        match auth_user_cmd {
            Ok(_) => {
                let password = &self.account.password.clone();
                auth_response = self.send_command("PASS", Some(password), None);
            }
            Err(_) => {
                let digest = self.get_apop_digest();
                let apop_param = format!("{} {}", self.account.username, digest);
                auth_response = self.send_command("APOP", Some(&apop_param), None);
            }
        }
        match auth_response {
            Ok(_) => {
                self.state = POP3State::TRANSACTION;
                Ok(POP3Data::LOGIN)
            }
            Err(e) => Err(Error::new(ErrorKind::Other, e)),
        }
    }

    fn list(&mut self, msgnum: Option<&str>) -> Result<POP3Data> {
        assert!(self.state == POP3State::TRANSACTION);
        self.send_command("LIST", msgnum, None).map(|msg| POP3Data::parse_list(&msg))
    }

    fn retr(&mut self, msg: EmailMetadata) -> Result<POP3Data> {
        assert!(self.state == POP3State::TRANSACTION);
        self.send_command("RETR", Some(&msg.msg_id.to_string()), Some(msg.msg_size))
            .map(|msg| POP3Data::parse_retr(&msg))
    }

    fn send_command(&mut self,
                    command: &str,
                    param: Option<&str>,
                    size: Option<u32>)
                    -> Result<Vec<String>> {
        lazy_static!{
            static ref RESPONSE: Regex = Regex::new(r"^(?P<status>\+OK|-ERR) (?P<statustext>.*)")
                .unwrap();
        }

        let is_multiline = match command {
            "LIST" => param.is_some(),
            "RETR" => true,
            _ => false,
        };

        let command = match param {
            Some(x) => format!("{} {}", command, x),
            None => command.to_string(),
        };

        println!("{}", command);
        self.stream.write_string(&command).unwrap();

        let size = size.unwrap_or(0);
        let response = self.read_response(is_multiline, size).unwrap();
        let status_line = response[0].clone();
        let response_groups = RESPONSE.captures(&status_line).unwrap();
        match response_groups.name("status") {
            Some("+OK") => Ok(response),
            Some("-ERR") => {
                Err(Error::new(ErrorKind::Other, response_groups["statustext"].to_string()))
            }
            _ => Err(Error::new(ErrorKind::Other, "Unparseable Response")),
        }
    }

    fn quit(&mut self) {
        let quit_command = "QUIT".to_owned();
        self.send_command(&quit_command, None, None).unwrap();
    }
}

fn save_message(path: &PathBuf, filename: &str, body: &[u8]) {
    let mut p = path.clone();
    p.push("new");
    p.push(filename.to_string());
    println!("{:?}", p);

    let mut handle = File::create(p).unwrap();
    handle.write_all(body).unwrap();
}


fn main() {
    let account = read_config("UdS");
    let mut connection = POP3Connection::new(account).unwrap();

    match connection.login() {
        Ok(_) => {}
        Err(e) => println!("Auth Failed: {}", e),
    };
    connection.stat().unwrap();
    let mlist: POP3Data = connection.list(None).unwrap();
    if let POP3Data::LIST { mailbox } = mlist {
        for mail in mailbox {
            let id = mail.msg_id;
            let msg = match connection.retr(mail).unwrap() {
                POP3Data::RETR { msg_data } => msg_data,
                _ => String::new(),
            };
            save_message(&connection.account.maildir,
                         id.to_string().as_ref(),
                         msg.as_ref());
        }
    }
    connection.quit();
}
